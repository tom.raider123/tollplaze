<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Auth::routes(['verify' => false,'register' => false]);

Route::get('/', function(){
	return redirect('login');
})->where(['url' => '|home'])->name('home');

if(env('API_URL')=="" || env('API_URL')==url('/')){
	Auth::routes(['register'=>false]);
	Route::get('index', 'HomeController@index')->name('index');
}

Route::group(['middleware' => ['frontend']], function(){	
	Route::get('/{url?}', 'HomeController@index')->where(['url' => '|home'])->name('home');
});

Route::group(['middleware' => ['auth']], function(){
	Route::get('access-denied', function(){
		return view('access-denied');
	})->name('access-denied');
        Route::resource('profile', 'ProfileController')->only(['index', 'store']);
		Route::group(['middleware' => ['check_permission'],'namespace'=>'Admin','prefix'=>'admin', 'as' => 'admin.'], function(){	

		Route::get('dashboard', 'DashboardController@index')->name('dashboard');
		Route::resource('profile', 'ProfileController')->only(['index', 'store']);
        Route::resource('settings', 'SettingsController')->only(['index', 'store']);

		// For Users
		Route::resources([
			'users' => 'UsersController',
		]);
		
        Route::post('users/getUsers', 'UsersController@getUsers')->name('users.getUsers');
		Route::get('users/status/{user_id}', 'UsersController@status')->name('users.status');	

       // For complainttype
		Route::resource('complainttype', 'ComplaintTypeController')->except(['show']);
		Route::post('complainttype/list', 'ComplaintTypeController@getComplaintType')->name('complainttype.getComplaintType');
		Route::get('complainttype/status/{id}', 'ComplaintTypeController@status')->name('complainttype.status');	

         // For roles
		Route::resource('roles','RoleController');
		Route::get('roles/destroy/{id}', 'RoleController@destroy')->name('roles.destroy');
		Route::get('roles/status/{id}', 'RoleController@status')->name('roles.status');
	
        // For Complaints
		Route::resource('complaints', 'ComplaintsController');
		Route::post('complaints/list', 'ComplaintsController@getComplaints')->name('complaints.getComplaints');
		Route::get('complaints/status/{id}', 'ComplaintsController@status')->name('complaints.status');
		Route::get('complaints/aprovelstatus/{id}', 'ComplaintsController@aprovelStatus')->name('complaints.aprovelstatus');	
		Route::get('complaints/rejectstatus/{id}', 'ComplaintsController@rejectStatus')->name('complaints.rejectStatus');	


		Route::resource('components','ComponentController');
    	Route::get('components/destroy/{id}', 'ComponentController@destroy')->name('components.destroy');
    	Route::get('components/status/{id}', 'ComponentController@status')->name('components.status');

    	Route::resource('vehicles','VehiclesController');
    	Route::post('vehicles/getVehicles', 'VehiclesController@getVehicles')->name('vehicles.getVehicles');
    	Route::get('vehicles/status/{id}', 'VehiclesController@status')->name('vehicles.status');

	});

});