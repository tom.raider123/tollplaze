<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// base path http://127.0.0.1:8000/api/auth/login
Route::group(['namespace'=>'API'], function(){
	Route::group([
	  'prefix' => 'auth'
	], function() {
		Route::post('register', 'AuthController@index');
		Route::post('login', 'AuthController@login');
		Route::post('sendOtp', 'AuthController@sendOtp');
		Route::post('forgotPassword', 'AuthController@forgotPassword');
		Route::post('resetPassword', 'AuthController@resetPassword');
		Route::group([
		  'middleware' => 'auth:api'
		], function() {
		    Route::get('logout', 'AuthController@logout');
		    Route::post('updateProfile', 'AuthController@updateProfile');
		});
	});

	// APIs that can access after login
	Route::group([
	  'middleware' => 'auth:api'
	], function(){
		//vehicle api
		Route::post('getAllVehicles', 'VehiclesController@getAllVehicles');
		Route::post('addVehicle', 'VehiclesController@addVehicle');
		Route::post('getVehicle', 'VehiclesController@getVehicle');
		Route::post('updateVehicle', 'VehiclesController@updateVehicle');
		//google api
		Route::post('getPlaces', 'ApiController@getPlaces'); 
		Route::post('getDirection', 'ApiController@getDirection');
		Route::post('getContact', 'ApiController@getContact'); 
        //complaint api
		Route::get('getAllComplaints', 'ComplaintsController@getAllComplaints');
		Route::post('addComplaint', 'ComplaintsController@addComplaint');
		Route::post('getComplaint', 'ComplaintsController@getComplaint');
		Route::post('getUserComplaints', 'ComplaintsController@getUserComplaints');
		// Route::post('updateComplaint', 'ComplaintsController@updateComplaint');
		//ForTolls
		Route::post('getNearbuyTolls', 'TollsController@getNearbuyTolls');


		//complainttype
		Route::get('getComplaintType', 'ComplaintTypeController@getComplaintType');
	
	});
	
	// APIs that can access without login
	// Route::get('public', 'ControllerName@functionName');
	// Route::post('public', 'ControllerName@functionName');
	// Write your routs here...
	// for store device data     
	Route::post('devices/readings', 'Webservices@storeDeviceReading');
	


});
