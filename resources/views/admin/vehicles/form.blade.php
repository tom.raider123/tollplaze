@extends('admin.layouts.valex_app')
@section('styles')
<link href="{{asset('template/valex-theme/plugins/select2/css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-datepicker3.standalone.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
    <div class="breadcrumb-header justify-content-between">
      <div class="left-content">
          <div>
            <h2 class="main-content-title tx-24 mg-b-1 mg-b-lg-1">Vehicle</h2>
          </div>
      </div>
    </div>
    <div class="row row-sm">
        <div class="col-xl-12">
            <div class="card">
                {!! Form::open(['method' => 'POST', 'route' => isset($vehicle->id)?['admin.vehicles.update',$vehicle->id]:['admin.vehicles.store'],'class' => 'form-horizontal','id' => 'frmSchedules', 'files' => true]) !!}
                @csrf
                @if(isset($vehicle->id))     
                @method('PUT')  
                @endif
                
                <div class="card-header py-3 cstm_hdr">
                    <h6 class="m-0 font-weight-bold text-primary">{{ isset($vehicle->id)?'Edit':'Add' }} Vehicle</h6>
                </div>
                <div class="card-body">
                    <div class="form-group {{$errors->has('vehical_make') ? config('constants.ERROR_FORM_GROUP_CLASS') : ''}}">
                        <label class="col-md-3 control-label" for="vehical_make">Vehicle Make<span style="color:red">*</span></label>
                        <div class="col-md-6">
                        {!! Form::text('vehical_make', old('vehical_make', isset($vehicle->vehical_make)?$vehicle->vehical_make:''), ['id'=>'vehical_make', 'class' => 'form-control', 'placeholder' => 'vehicle make']) !!}
                            <span id="sale_id_error"></span>
                            @if($errors->has('vehical_make'))
                            <strong for="vehical_make" class="help-block">{{ $errors->first('vehical_make') }}</strong>
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{$errors->has('vehical_model') ? config('constants.ERROR_FORM_GROUP_CLASS') : ''}}">
                        <label class="col-md-3 control-label" for="vehical_model">Vehicle Model <span style="color:red">*</span></label>
                        <div class="col-md-6">
                        {!! Form::text('vehical_model', old('vehical_model', isset($vehicle->vehical_model)?$vehicle->vehical_model:''), ['id'=>'vehical_model', 'class' => 'form-control', 'placeholder' => 'vehicle model']) !!}
                            @if($errors->has('vehical_model'))
                            <strong for="vehical_model" class="help-block">{{ $errors->first('vehical_model') }}</strong>
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{$errors->has('vehical_number') ? config('constants.ERROR_FORM_GROUP_CLASS') : ''}}">
                        <label class="col-md-3 control-label" for="vehical_number">Vehicle Number<span style="color:red">*</span></label>
                        <div class="col-md-6">

                        {!! Form::text('vehical_number', old('vehical_number', isset($vehicle->vehical_number)?$vehicle->vehical_number:''), ['id'=>'vehical_number', 'class' => 'form-control', 'placeholder' => 'vehicle number']) !!}

                            @if($errors->has('vehical_number'))
                            <strong for="time" class="help-block">{{ $errors->first('vehical_number') }}</strong>
                            @endif
                        </div>
                    </div>
                    
                </div>  
                <div class="card-footer">
                    <button type="submit" class="btn btn-responsive btn-primary">{{ __('Submit') }}</button>
                    <a href="{{route('admin.vehicles.index')}}"  class="btn btn-responsive btn-secondary">{{ __('Cancel') }}</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>        
</div>
<!-- /.container-fluid -->
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-validation/dist/additional-methods.min.js') }}"></script>
<script src="{{ asset('template/valex-theme/plugins/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('js/datepicker/bootstrap-datepicker.min.js') }}"></script>

<script type="text/javascript">
jQuery(document).ready(function(){
    //$('[name="role_id[]"]').selectpicker();
    $('.select2').select2({
        placeholder: '-Select Customer-'
    });

    $('.user_id').select2({
        placeholder: '-Select User-'
    });

    $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            orientation: 'bottom',
            autoclose: true
    });

    jQuery('#frmSchedules').validate({
        rules: {
            time: {
                required: true
            },
            date: {
                required: true
            },
            sale_id: {
                required: true
            },
            user_id: {
                required: true
            },
        }
    });
});
</script>
@endsection