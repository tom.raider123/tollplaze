@extends('admin.layouts.valex_app')
@section('content')
<!-- Page Heading -->
<div class="container"> 
    <div class="breadcrumb-header justify-content-between">
        <div class="left-content">
            <div>
              <h2 class="main-content-title tx-24 mg-b-1 mg-b-lg-1">Vehicle</h2>
            </div>
        </div>
    </div>
     <div class="row row-sm">
        <div class="col-xl-12">
        <!-- Content Row -->
            <div class="card">
                <div class="card-header py-3 cstm_hdr">
                    <h6 class="m-0 font-weight-bold text-primary">Vehicle Details</h6>
                </div>
                <div class="card-body">
                                    <!-- <div class="row mg-t-20">
                                        <div class="col-md">
                                            <div class="billed-to">
                                                <span><b>Vehicle Make :</b>  {{ isset($vehicle->vehical_make)?$vehicle->vehical_make:'' }}  </span>
                                                <br>
                                                <span><b>Vehicle Model:</b> {{ isset($vehicle->vehical_model)?$vehicle->vehical_model:'' }} </span>
                                                <br>
                                                <span><b>Vehicle Number  :</b> {{ isset($vehicle->vehical_number)?$vehicle->vehical_number:'' }} </span>
                                                <br>
                                                <span><b>Added By   :</b>  {{ isset($vehicle->getuser->name)?$vehicle->getuser->name:'' }} 
                                                
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="table-responsive mg-t-40">
                                        <table class="table table-invoice border text-md-nowrap mb-0">
                                            <thead>
                                                <tr>
                                                    <th class="wd-20p">Vehicle Make </th>
                                                    <th class="wd-20p">Vehicle Model</th>
                                                    <th class="wd-20p">Vehicle Number</th>
                                                    <th class="wd-20p">Added By</th>
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <tr>
                                               <td>{{ isset($vehicle->vehical_make)?$vehicle->vehical_make:'' }}</td>
                                               <td> {{ isset($vehicle->vehical_model)?$vehicle->vehical_model:'' }}</td>
                                               <td>{{ isset($vehicle->vehical_number)?$vehicle->vehical_number:'' }}</td>
                                               <td>{{ isset($vehicle->getuser->name)?$vehicle->getuser->name:'' }}</td>

                                               </tr>
                                            </tbody>
                                        </table>
                                    </div>
                </div>  
                <div class="card-footer">
                    <a href="{{route('admin.vehicles.index')}}"  class="btn btn-secondary">Back</a>
                </div>
               
            </div>
        </div>
    </div>
</div>
@endsection


