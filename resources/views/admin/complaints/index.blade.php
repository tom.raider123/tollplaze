@extends('admin.layouts.valex_app')
@section('styles')
<link href="{{asset('template/valex-theme/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{asset('template/valex-theme/plugins/datatable/css/responsive.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{asset('template/valex-theme/plugins/datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{asset('template/valex-theme/plugins/datatable/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('template/valex-theme/plugins/datatable/css/responsive.dataTables.min.css')}}" rel="stylesheet">
@endsection

@section('content')
<!-- Begin Page Content -->
<div class="container">
  <div class="breadcrumb-header justify-content-between">
      <div class="left-content">
          <div>
            <h2 class="main-content-title tx-24 mg-b-1 mg-b-lg-1">Complaints</h2>
          </div>
      </div>
  </div>
  <div class="row row-sm">
    <div class="col-xl-12">
      <div class="card">
        <div class="card-header py-3 cstm_hdr">
             <h6 class="m-0 font-weight-bold text-primary">Complaints</h6>
            <!-- <a href="{{route('admin.complainttype.create')}}" class="btn btn-sm btn-icon-split float-right btn-outline-warning">
                <span class="icon text-white-50">
                  <i class="fas fa-plus"></i>
                </span>
                <span class="text">Add Type</span>
            </a> -->
           
        </div>
        <div class="card-body">

        <div class="well mb-3">
                            {!! Form::open(['method' => 'POST', 'class' => 'form-inline', 'id' => 'frmFilter']) !!}
                            <?php if(Auth::user()->roles->first()->id == config('constants.ROLE_TYPE_SUPERADMIN_ID')){   ?>

                            <?php } ?>   
                    
                            <div class="form-group mr-sm-2 mb-2">
                                <select name="status" id="status" class="form-control">
                                    <option value="">-Select Status-</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>                
                            </div>
                            <div class="form-group mr-sm-2 mb-2">
                                <select name="aprovel_status" id="aprovel_status" class="form-control">
                                    <option value="">-Select Aprovel Status-</option>
                                    <option value="0">Pending</option>
                                    <option value="1">Approved</option>
                                    <option value="2">Rejected</option>
                                </select>                
                            </div>  
                            <button type="submit" class="btn btn-responsive btn-primary mr-sm-2 mb-2">{{ __('Filter') }}</button>
                            <a href="javascript:;" onclick="resetFilter();" class="btn btn-responsive btn-danger mb-2">{{ __('Reset') }}</a>
                            {!! Form::close() !!}
                    </div> 
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0" id="conplaints">
                    <thead>
                        <tr>
                            <th>Added By</th> 
                            <th>Added</th> 
                            <th>Location</th>           
                            <th>Reason</th>
                            <th>Comments</th> 
                            <th>Image</th>                       
                            <th>Video</th> 
                            <th>Approvel status</th>
                            <th>Status</th>
                            <th>Action</th> 
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                           <th>Added By</th> 
                            <th>Added</th> 
                            <th>Location</th>           
                            <th>Reason</th>
                            <th>Comments</th> 
                            <th>Image</th>                       
                            <th>Video</th> 
                            <th>Approvel status</th>
                            <th>Status</th>
                            <th>Action</th>                 
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>    
<!-- /.container-fluid -->
@endsection

@section('scripts')
<script src="{{ asset('template/valex-theme/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('template/valex-theme/plugins/datatable/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ asset('template/valex-theme/plugins/datatable/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('template/valex-theme/plugins/datatable/js/responsive.dataTables.min.js') }}"></script>
<script src="{{ asset('template/valex-theme/plugins/datatable/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('template/valex-theme/plugins/datatable/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('template/valex-theme/plugins/datatable/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('template/valex-theme/plugins/datatable/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('template/valex-theme/plugins/datatable/js/vfs_fonts.js') }}"></script>
<!-- export btn -->
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#frmFilter :input:not(:button, [type="hidden"])').val('');
        getComplaints();
    });
    jQuery('#frmFilter').submit(function(){
        getComplaints();
        return false;
    });
    function resetFilter(){
    jQuery('#frmFilter :input:not(:button, [type="hidden"])').val('');
    getComplaints();
}
    function getComplaints(){        
        var status = jQuery('#frmFilter [name=status]').val();
        var aprovel_status = jQuery('#frmFilter [name=aprovel_status]').val();

        jQuery('#conplaints').dataTable().fnDestroy();
        jQuery('#conplaints tbody').empty();
        var table=jQuery('#conplaints').DataTable({
            processing: true,
            serverSide: true,
            iDisplayLength:50,
            ajax: {
                url: '{{ route('admin.complaints.getComplaints') }}',
                method: 'POST',
                data: {  
                status:status,
                aprovel_status:aprovel_status,
                }
            },
            lengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50,100,"All"]
            ],
            columns: [              
                {data: 'user_id', name: 'user_id',class: 'text-center'},
                {data: 'created_at', name: 'created_at',class: 'text-center'},                
                {data: 'location', name: 'location',class: 'text-center'},             
                {data: 'reason', name: 'reason',class: 'text-center'},
                {data: 'comments', name: 'comments',class: 'text-center'},
                {data: 'evidence_image', name: 'evidence_image',class: 'text-center'},
                {data: 'evidence_video', name: 'evidence_video',class: 'text-center'},
                {data: 'status', name: 'status',"width": "10%",class: 'text-center'},
                {data: 'is_active', name: 'is_active', class: 'text-center',"width": "5%"},             
                {data: 'action', name: 'action', orderable: false, searchable: false ,class: 'text-center',"width": "5%"}
                           
            ],           
            order: [[0, 'asc']], 
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_',
            },  
        }); 
    
    }      
</script>
@endsection