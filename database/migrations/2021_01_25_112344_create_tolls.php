<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTolls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tolls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('toll_name')->nullable();
            $table->string('toll_lat')->nullable();
            $table->string('toll_long')->nullable();
            $table->string('toll_city')->nullable();
            $table->string('toll_state')->nullable();
            $table->string('toll_piu')->nullable();
            $table->string('toll_ro')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tolls');
    }
}
