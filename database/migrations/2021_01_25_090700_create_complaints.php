<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('location')->nullable();
            $table->string('reason')->nullable();
            $table->text('comments')->nullable();
            $table->text('evidence_image')->nullable();
            $table->text('evidence_voice')->nullable();
            $table->text('evidence_video')->nullable();
            $table->string('is_active')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
