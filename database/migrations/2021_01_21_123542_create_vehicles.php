<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('vehical_make')->nullable();
            $table->string('vehical_model')->nullable();
            $table->string('vehical_number')->nullable();
            $table->text('vehical_image')->nullable();
            $table->string('is_active')->nullable();
            $table->string('added_by')->nullable();
            $table->timestamps();
            // $table->foreign('added_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
