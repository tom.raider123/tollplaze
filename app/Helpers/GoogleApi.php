<?php

namespace App\Helpers;
use Carbon\Carbon;

class GoogleApi
{
    public function __construct()
    {
        
    }
    public function getNearbuyPlaces($lat,$long,$type)
    {
        try {
            $api_key=env('GOOGLE_MAP_ACCESS_KEY');
            $url="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=1500&type=$type&keyword=$type&key=$api_key";     
            $cSession = curl_init(); 
            curl_setopt($cSession,CURLOPT_URL,$url);
            curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($cSession,CURLOPT_HEADER, false); 
            $result=curl_exec($cSession);
            curl_close($cSession);
            return  $result;
            }
            //catch exception
            catch (\Exception $e) {
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                return response()->json($response);
            }
    }
    public function getDirection($start,$end)
    {
        try {
          $api_key=$api_key=env('GOOGLE_MAP_ACCESS_KEY');
  $url="https://maps.googleapis.com/maps/api/directions/json?origin=$start&destination=$end&key=$api_key";     
            $cSession = curl_init(); 
            curl_setopt($cSession,CURLOPT_URL,$url);
            curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($cSession,CURLOPT_HEADER, false); 
            $result=curl_exec($cSession);
            curl_close($cSession);
            return  $result;
            }
            //catch exception
            catch (\Exception $e) {
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                return response()->json($response);
            }
    }  
    
    

    public function getPlaceDetail($place_id){
        try {
            $api_key=$api_key=env('GOOGLE_MAP_ACCESS_KEY');
            $url="https://maps.googleapis.com/maps/api/place/details/json?place_id=$place_id&fields=name,rating,formatted_phone_number&key=$api_key";
              $cSession = curl_init(); 
              curl_setopt($cSession,CURLOPT_URL,$url);
              curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
              curl_setopt($cSession,CURLOPT_HEADER, false); 
              $result=curl_exec($cSession);
              curl_close($cSession);
              return  $result;
              }
              //catch exception
              catch (\Exception $e) {
                  $response['status'] = 500;
                  $response['message'] = $e->getMessage();
                  return response()->json($response);
              }


    }
}
