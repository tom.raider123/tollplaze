<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toll extends Model
{
    protected $fillable = [
        'toll_name','toll_lat','toll_long','toll_city','toll_state','toll_piu','toll_ro'
    ];
}
