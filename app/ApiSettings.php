<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiSettings extends Model
{
    protected $fillable = [
        'device_type','app_version','api_version','is_active'
    ];
}
