<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Complaint;
use DataTables;
use Form;
use Str;


class ComplaintsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.complaints.index');
    }

    public function getComplaints(Request $request){
        $complaint = Complaint::with('getuser')->select([\DB::raw(with(new Complaint)->getTable().'.*')])->groupBy('id');
        $status = $request->input('status');
        if($status !='')
        $complaint->where('is_active', $status);
        $aprovel_status = $request->input('aprovel_status');
        if($aprovel_status !='')
        $complaint->where('status',$aprovel_status);  
        
        
        return DataTables::of($complaint)               
            ->editColumn('user_id', function ($complaint){
                return $complaint->getuser->mobile_number;
            })
            ->editColumn('created_at', function ($complaint){
                return $complaint->created_at->diffForHumans();
            })
            ->editColumn('location', function ($complaint){
                return  Str::limit($complaint->location, 100);
            })
            ->editColumn('reason', function ($complaint){
                return Str::limit($complaint->reason, 100);
            })
            ->editColumn('comments', function ($complaint){
                return Str::limit($complaint->comments, 100);
            })  
            ->editColumn('evidence_image', function ($complaint){
                if($complaint->evidence_image){
                    $url=url('/uploads/users',$complaint->evidence_image);
                    return '<img style="max-height: 100px;max-width:100px;"  src="'.$url.'"/>';
                }
                return '';
              
            })
            ->editColumn('evidence_video', function ($complaint){
                if($complaint->evidence_video){
                    $url=url('/uploads/users',$complaint->evidence_video);
                    return '<video style="max-height:100px;max-width:100px;" src="'.$url.'" controls ></video>';  
                }
                return '';
              
            })
            ->editColumn('status', function ($complaint){
                if($complaint->status == false ){
                    return "<a href='".route('admin.complaints.rejectStatus',$complaint->id)."'><span class='btn btn-danger btn-sm'><i class='fas fa-times'></i></span></a>  <a href='".route('admin.complaints.aprovelstatus',$complaint->id)."'><span class='btn btn-success btn-sm'><i class='fas fa-check'></i></span></a>";
                }else{
                    if($complaint->status == 1 ){
                        return "<span class='badge badge-success'>Approved</span>";
                    }elseif($complaint->status == 2){
                        return "<span class='badge badge-danger'>Rejected</span>";
                    }
                    else{
                        return "<span class='badge badge-warning'>Pending</span>";
                    }
                }
            })
            ->editColumn('is_active', function ($complaint){
                if($complaint->is_active == TRUE ){
                    return "<a href='".route('admin.complaints.status',$complaint->id)."'><span class='badge badge-success'>Active</span></a>";
                }else{
                    return "<a href='".route('admin.complaints.status',$complaint->id)."'><span class='badge badge-danger'>Inactive</span></a>";
                }
            }) 
            ->addColumn('action', function ($complaint){
                return
                    // edit
                    // '<a href="'.route('admin.complaints.edit',[$complaint->id]).'" class="btn btn-success btn-circle btn-sm"><i class="fas fa-edit"></i></a> '.
                    // Delete
                    Form::open(array(
                        'style' => 'display: inline-block;',
                        'method' => 'DELETE',
                        'onsubmit'=>"return confirm('Do you really want to delete?')",
                        'route' => ['admin.complaints.destroy', $complaint->id])).
                    ' <button type="submit" class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></button>'.
                    Form::close();
            })
            ->rawColumns(['status','aprovel','is_active','action','evidence_image','evidence_video'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Complaint $Complaint)
    {
        $Complaint->delete();
        $request->session()->flash('danger',__('global.messages.delete'));
        return redirect()->route('admin.complaints.index');
    }

    public function status(Request $request, $complaint_id=null){
        $complaint = Complaint::findOrFail($complaint_id);
        if (isset($complaint->is_active) && $complaint->is_active==FALSE) {
            $complaint->update(['is_active'=>TRUE]);
            $request->session()->flash('success',__('global.messages.activate'));
        }else{
            $complaint->update(['is_active'=>FALSE]);
            $request->session()->flash('danger',__('global.messages.deactivate'));
        }
        return redirect()->route('admin.complaints.index');
    }
    


    public function aprovelStatus(Request $request, $complaint_id=null){
        $complaint = Complaint::findOrFail($complaint_id);
            $complaint->update(['status'=>true]);
            $request->session()->flash('success',__('global.messages.approve'));
        return redirect()->route('admin.complaints.index');
    }
    public function rejectStatus(Request $request, $complaint_id=null){
        $complaint = Complaint::findOrFail($complaint_id);
            $complaint->update(['status'=>2]);
            $request->session()->flash('danger',__('global.messages.reject'));
        return redirect()->route('admin.complaints.index');
    }
   
}
