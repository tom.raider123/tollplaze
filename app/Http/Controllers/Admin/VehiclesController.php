<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Schedule;
use Validator;
use Auth;
use DataTables;
use Config;
use Form;
use DB;
use App\Vehicles;

class VehiclesController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){  
    
        return view('admin/vehicles/index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getVehicles(Request $request){

        $vehicles=Vehicles::with('getuser')->select([\DB::raw(with(new Vehicles)->getTable().'.*')])->groupBy('id');
        // dd($vehicles);

        // $sale_id = intval($request->input('sales_id'));
        // if($sale_id > 0) 
        //     $schedules->where('sale_id', $sale_id); 
           
        $status = $request->input('status');
        if($status !='') 
            $vehicles->where('is_active', $status); 

        // $date = $request->input('date');
        // if($date!=''){
        //    $date = date('Y-m-d', strtotime($date));
        //    $schedules->whereDate('datetime', '=', $date);
        // }
        
        return DataTables::of($vehicles)
            ->editColumn('vehical_make', function($vehicles){
                return $vehicles->vehical_make;
            })            
            ->editColumn('vehical_model', function ($vehicles){
               return $vehicles->vehical_model;
            })
            ->editColumn('vehical_number', function($vehicles){
                return $vehicles->vehical_number;
            })
            ->editColumn('added_by', function($vehicles){
              return isset($schedule->sale->email)?$schedule->sale->email:'';
          })
            ->filterColumn('vehical_make', function ($query, $keyword){
                $keyword = strtolower($keyword);
                  	 $query->whereRaw("vehical_make like ?", ["%$keyword%"]);
            })
            ->filterColumn('vehical_model', function ($query, $keyword) {
                $keyword = strtolower($keyword);
                  	 $query->whereRaw("vehical_model like ?", ["%$keyword%"]);
            })
            ->filterColumn('vehical_number', function ($query, $keyword){
                $keyword = strtolower($keyword);
                $query->whereRaw("vehical_number like ?", ["%$keyword%"]);        
            })
        ->editColumn('is_active', function ($vehicles) {
          if($vehicles->is_active == TRUE )
          {
              return "<a href='".route('admin.vehicles.status',$vehicles->id)."'><span class='badge badge-success'>Active</span></a>";
          }else{
              return "<a href='".route('admin.vehicles.status',$vehicles->id)."'><span class='badge badge-danger'>Inactive</span></a>";
          }
      })

            ->addColumn('action', function ($vehicles){
                return
                    // edit
                    '<a href="'.route('admin.vehicles.show',[$vehicles->id]).'" class="btn btn-success btn-circle btn-sm"><i class="fa fa-eye"></i></a> <a href="'.route('admin.vehicles.edit',[$vehicles->id]).'" class="btn btn-primary btn-circle btn-sm"><i class="fas fa-edit"></i></a> '.
                    // Delete
                      Form::open(array(
                                  'style' => 'display: inline-block;',
                                  'method' => 'DELETE',
                                   'onsubmit'=>"return confirm('Do you really want to delete?')",
                                  'route' => ['admin.vehicles.destroy', $vehicles->id])).
                      ' <button type="submit" class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></button>'.
                      Form::close();
            })
            ->rawColumns(['action','is_active'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
     
        return view('admin.vehicles.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $rules = [
            'vehical_make'  => 'required', 
            'vehical_model'  => 'required', 
            'vehical_number'     => 'required|alpha_num', 
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->passes()){
            $data = $request->all();
            $data['added_by']=Auth::user()->id;
            $data['is_active']=true;
            Vehicles::create($data);
            $request->session()->flash('success',__('global.messages.add'));
            return redirect()->route('admin.vehicles.index');
        }else{
            return redirect()->back()->withErrors($validator)->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($vehicle_id){
      $vehicle = Vehicles::with('getuser')->where('id',$vehicle_id)->first();
    	return view('admin.vehicles.show', compact('vehicle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($vehicle_id){
     
    	$vehicle = Vehicles::findOrFail($vehicle_id);
      
        return view('admin.vehicles.form', compact('vehicle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $vehicle_id){
        $vehicle = Vehicles::findOrFail($vehicle_id);
        $rules =[
            'vehical_make'  => 'required', 
            'vehical_model' => 'required', 
            'vehical_number' => 'required|alpha_num',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()){
            $data = $request->all();
            $vehicle->update($data);
            $request->session()->flash('success',__('global.messages.update'));
            return redirect()->route('admin.vehicles.index');
        }else{
            return redirect()->back()->withErrors($validator)->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($vehicle_id){
      $vehicle = Vehicles::findOrFail($vehicle_id);
      $vehicle->delete();
      session()->flash('danger',__('global.messages.delete'));
      return redirect()->route('admin.vehicles.index');
    }
    public function status($id){
      $vehicles = Vehicles::findOrFail($id);
      if(isset($vehicles->is_active) && $vehicles->is_active==FALSE){
          $vehicles->update(['is_active'=>TRUE]);
          session()->flash('success',__('global.messages.activate'));
      }else{
          $vehicles->update(['is_active'=>FALSE]);
          session()->flash('danger',__('global.messages.deactivate'));
      }
      return redirect()->route('admin.vehicles.index');
    }
}
