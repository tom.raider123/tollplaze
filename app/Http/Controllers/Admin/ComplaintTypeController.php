<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Form;
use App\ComplaintType;
use File;

class ComplaintTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.complainttype.index');
    }

      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getComplaintType(Request $request){
        $complainttype = ComplaintType::query(); 
        $complainttype->select([\DB::raw(with(new ComplaintType)->getTable().'.*')])->groupBy('id');          

        return DataTables::of($complainttype)               
            ->editColumn('is_active', function ($complainttype){
                if($complainttype->is_active == TRUE ){
                    return "<a href='".route('admin.complainttype.status',$complainttype->id)."'><span class='badge badge-success'>Active</span></a>";
                }else{
                    return "<a href='".route('admin.complainttype.status',$complainttype->id)."'><span class='badge badge-danger'>Inactive</span></a>";
                }
            })
            ->editColumn('image', function ($complainttype){
                $url=url('/uploads/setting',$complainttype->image);
                return '<img src="'.$url.'"/>';
            })   

            ->addColumn('action', function ($complainttype) {
                return
                    // edit
                    '<a href="'.route('admin.complainttype.edit',[$complainttype->id]).'" class="btn btn-success btn-circle btn-sm"><i class="fas fa-edit"></i></a> '.
                    // Delete
                    Form::open(array(
                        'style' => 'display: inline-block;',
                        'method' => 'DELETE',
                        'onsubmit'=>"return confirm('Do you really want to delete?')",
                        'route' => ['admin.complainttype.destroy', $complainttype->id])).
                    ' <button type="submit" class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></button>'.
                    Form::close();
            })
            ->rawColumns(['media.name','is_active','action','image'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.complainttype.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $ComplaintType = ComplaintType::get();
        $rules = [
            'title'=>'required|unique:'.with(new ComplaintType)->getTable(),
            'image'=>'file|mimes:jpeg,png,jpg',
        ];
        $request->validate($rules);
        $data = $request->all();
        $type_image = $request->file('image');
        if($type_image){
            $path = public_path(config('constants.SETTING_IMAGE_URL'));
            $type_imageName = time().'.'.$type_image->getClientOriginalExtension();
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            $uploadResponse = $type_image->move($path, $type_imageName);
            $data['image'] = $type_imageName;
        }
        $type=ComplaintType::create($data);
        $request->session()->flash('success',__('global.messages.add'));
        return redirect()->route('admin.complainttype.index');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($complainttype_id)
    {
        $complainttype = ComplaintType::findOrFail($complainttype_id);
        return view('admin.complainttype.form',compact('complainttype'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Complainttype $Complainttype)
    {
        $rules = [            
            'title'=>'required|unique:'.with(new ComplaintType)->getTable().',title,'.$Complainttype->getKey(),
            'image'=>'file|mimes:jpeg,png,jpg',
        ];
        $request->validate($rules);
        $data = $request->all();
        $type_image = $request->file('image');
        if($type_image){
            $path = public_path(config('constants.SETTING_IMAGE_URL'));
            $type_imageName = time().'.'.$type_image->getClientOriginalExtension();
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            $uploadResponse = $type_image->move($path, $type_imageName);
            $data['image'] = $type_imageName;
        }
        $Complainttype->update($data);   
        $request->session()->flash('success',__('global.messages.update'));
        return redirect()->route('admin.complainttype.index');
    }

    /**
     * Change status the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
    */
    public function status(Request $request, $country_id=null){
        $country = ComplaintType::findOrFail($country_id);
        if (isset($country->is_active) && $country->is_active==FALSE) {
            $country->update(['is_active'=>TRUE]);
            $request->session()->flash('success',__('global.messages.activate'));
        }else{
            $country->update(['is_active'=>FALSE]);
            $request->session()->flash('danger',__('global.messages.deactivate'));
        }
        return redirect()->route('admin.complainttype.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request,Complainttype $Complainttype)
     {
            $Complainttype->delete();
            $request->session()->flash('danger',__('global.messages.delete'));
            return redirect()->route('admin.complainttype.index');
     }
}
