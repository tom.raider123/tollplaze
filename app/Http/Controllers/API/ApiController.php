<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\GoogleApi;
use Validator;
use App\Helpers\Messages;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->places = new GoogleApi();
    }
    public function getPlaces(Request $request){
        try{ 
        $rules=['lat' => 'required','long' => 'required','type' => 'required'];
        $messages = [];
        $validator = Validator::make($request->all(), $rules, $messages);        
        if ($validator->fails()){ 
        return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);    
        } 
        $places=$this->places->getNearbuyPlaces($request->lat,$request->long,$request->type);
        // $res=json_decode($places, true);
   return $places;
   
    }catch (\Exception $e) {
        $response['status'] = 500;
        $response['message'] = $e->getMessage();
        return response()->json($response);
    }
    }
    public function getDirection(Request $request){
        $rules=['start' => 'required','end' => 'required'];
        $messages = [];
        $validator = Validator::make($request->all(), $rules, $messages);        
        if ($validator->fails()){ 
        return response()->json(['message'=>$validator->errors()->first()]);    
        } 
        $places=$this->places->getDirection($request->start,$request->end);
        return $places;
    }
    public function getContact(Request $request){
        $rules=['place_id' => 'required'];
        $messages = [];
        $validator = Validator::make($request->all(), $rules, $messages);        
        if ($validator->fails()){ 
        return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);    
        } 
        $contact=$this->places->getPlaceDetail($request->place_id);
        return $contact;
    }
}
