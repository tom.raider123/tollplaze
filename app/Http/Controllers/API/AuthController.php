<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use Validator;
use Auth;
use File;

use App\Notifications\SendOTP;
use App\User;
use App\PasswordReset;
use App\Helpers\Messages;






// use App\Models\Passport\AuthCode;
// use App\Models\Passport\Client;
// use App\Models\Passport\PersonalAccessClient;
// use App\Models\Passport\Token;

class AuthController extends Controller
{
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 

    public function __construct()
    {
        $this->messages = new Messages();
    }
    public function index(Request $request){ 
        try{
        $validator = Validator::make($request->all(),[ 
            'mobile_number' => 'required|numeric|unique:'.with(new User)->getTable().',mobile_number', 
            'email' => 'required|email|unique:'.with(new User)->getTable().',email',
            // 'password'=>'required'
        ]);
        if ($validator->fails()){ 
            return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);            
        }
        $input = array_map('trim', $request->all());
        // $otp=mt_rand(100000, 999999);
        // $input['password'] = bcrypt(999999);
        $user = User::create($input); 
        if($user){
            if ($user)
            // $msg=$this->messages->sendmessage($otp,$request->mobile_number);
            // $user->notify(new SendOTP('Otp',$otp));
            $user->assignRole(config('constants.ROLE_TYPE_USER_ID'));
            $response['status'] = 200; 
            $response['message'] = "You has been successfully registered.";
            return response()->json($response);
        }else{
            return response()->json(['status'=>500,'message'=>'Something wrong in registration.']);
        }
    }catch (\Exception $e){
        $response['status'] = 500;
        $response['message'] = $e->getMessage();
        return response()->json($response);
    }
    }

    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */
    
     public function sendOtp(Request $request){
         try{ 
        $rules=['mobile_number' => 'required'];
            $messages = [];
            $validator = Validator::make($request->all(), $rules, $messages)->setAttributeNames(['email'=>'email or username']);        
            if ($validator->fails()) { 
            return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);    
            } 
             $mobile=$request->mobile_number;
            //  $otp=mt_rand(100000, 999999);
            //  $user->notify(new SendOTP('Otp',$otp));
            // $msg=$this->messages->sendmessage($otp,$request->mobile_number);
             $user=User::where('mobile_number',$mobile)->update(["password"=>bcrypt(999999)]);
             if($user){
                $response['status'] = 200; 
                $response['message'] = "Otp sent successfully.";
                return response()->json($response);
             }
             else{
                $response['status'] = 400; 
                $response['message'] = "Please enter correct mobile number.";
                return response()->json($response);
             }
            }catch (\Exception $e){
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                return response()->json($response);
            }
     }
    public function login(Request $request){ 
        try{
        $rules =   ['mobile_number' => 'required', 
                    'password' => 'required'];
        $messages = [];
        $validator = Validator::make($request->all(), $rules, $messages)->setAttributeNames(['email'=>'email or username']);        
        if ($validator->fails()) { 
            return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);    
        }       
        // $mobile_number = $request->input('mobile_number');
        $credentials = $request->only('mobile_number', 'password');
        if(Auth::attempt($credentials)){
            $user = Auth::user();
            if($user->hasRole(config('constants.ROLE_TYPE_SUPERADMIN_ID')))
                return response()->json(['status'=>403,'message'=>trans('auth.failed')]);
            if($user->is_active==false){
                return response()->json(['status'=>403,'message'=>trans('auth.noactive')]);
            }
            // For store access token of user
            $tokenResult = $user->createToken('Login Token');
            $token = $tokenResult->token;
            $response['status'] = 200; 
            $response['message'] = "Logged in successfully.";
            $response['user'] = $user->getUserDetail();
            $response['access_token'] = $tokenResult->accessToken;
            $response['token_type'] = 'Bearer';
            $response['expires_at'] = Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString();
            return response()->json($response); 
        }else{
            return response()->json(['status'=>401 ,'message'=>trans('auth.failed')]); 
        }
    }catch (\Exception $e){
        $response['status'] = 500;
        $response['message'] = $e->getMessage();
        return response()->json($response);
    }
    }
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request){
        try{ 
        $request->user()->token()->revoke();
        $response['status'] = 200;  
        $response['message'] = "Successfully logged out";
        return response()->json($response);
        }
        catch (\Exception $e){
            $response['status'] = 500;
            $response['message'] = $e->getMessage();
            return response()->json($response);
        }
    }
    /** 
     * forgotPassword api 
     * 
     * @return \Illuminate\Http\Response 
    */ 
    public function forgotPassword(Request $request){        
        $rules=['email' => 'required'];
        $credentials= ['email' => $request->get('email')];
        $messages=[
            'email.required' => "The email field is required.",
        ];
        $validator = Validator::make($request->all(), $rules,$messages);
        if ($validator->fails()) { 
            return response()->json(['message'=>$validator->errors()->first()]);    
        }
        $user=User::where($credentials)->first();
        if($user){
            PasswordReset::where('email', $user->email)->delete();
            $passwordReset = PasswordReset::create(
                [
                    'email' => $user->email,
                    'token' => str_random(6)
                ]
            );
            if ($user && $passwordReset)
                $user->notify(
                    new SendOTP('forget_password', $passwordReset->token)
                );
            $response['status'] = true; 
            $response['response'] = array();
            $response['message'] = "We have sent a verification code on your email";
            return response()->json($response); 
        }else{
            return response()->json(['message'=>'Account details not found.']);
        }
    }

    /** 
     * resetPassword api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function resetPassword(Request $request){
        try{ 
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'token' => 'required', 
            'password' => 'required|confirmed'
        ]);
        if ($validator->fails()) { 
            return response()->json(['message'=>$validator->errors()->first()]);            
        }
        $user = User::where('email', $request->input('email'))->first();
        if (!$user)
            return response()->json([
                'message' => 'We can\'t find a user with that e-mail address.'
            ]);
        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ]);
        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->where('email', $passwordReset->email)->delete();
        $response['status'] = 200;
        $response['message'] = "Update Password successfully.";
        return response()->json($response); 
            }catch (\Exception $e){
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                return response()->json($response);
            }
    }
    /**
     * update user
     *
     * @return [string] message
     */
    public function updateProfile(Request $request){

        try{
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            // 'name' => 'required',
            'email' => 'required|email|unique:'.with(new User)->getTable().',email,'.$user->getKey(),
            // 'profile_picture' => 'image'
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);
        }
        $data = $request->all();
        $userfile = $request->file('profile_picture');
        if($userfile){
            $path = public_path(config('constants.USERS_UPLOADS_PATH'));
            $profile_pictureName = time().'.'.$userfile->getClientOriginalExtension();
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            $uploadResponse = $userfile->move($path, $profile_pictureName);
            if(isset($user->profile_picture) && $user->profile_picture!='' && file_exists($path.$user->profile_picture)){
                File::delete($path.$user->profile_picture);
            }
            $data['profile_picture'] = $profile_pictureName;
        }
        $user->update($data);
        $response['status'] = 200;  
        $response['user'] = $user->getUserDetail();
        $response['message'] = "Profile updated Successfully.";
        return response()->json($response);
    }catch (\Exception $e){
        $response['status'] = 500;
        $response['message'] = $e->getMessage();
        return response()->json($response);
    }
    }
}
