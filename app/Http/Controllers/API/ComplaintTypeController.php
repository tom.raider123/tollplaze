<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ComplaintType;

class ComplaintTypeController extends Controller
{
   function getComplaintType(){
try{
    $complainttype=ComplaintType::where('is_active',true)->get();
    if(!empty($complainttype)){
        $response['status'] = 200;
        $response['data'] = $complainttype;
        $response['message'] = "Success";
        return response()->json($response);
    }
    else{
        $response['status'] = 204;
        $response['data'] = [];
        $response['message'] = "Success";
        return response()->json($response);

    }
}catch (\Exception $e) {
    $response['status'] = 500;
    $response['message'] = $e->getMessage();
    return response()->json($response);
}
   }
}

