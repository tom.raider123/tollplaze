<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Vehicles;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use Validator;
use Auth;
use File;

use App\Notifications\SendOTP;
use App\User;
use App\PasswordReset;
use App\Helpers\Messages;

class VehiclesController extends Controller
{


    public function getAllVehicles(Request $request){
        try {
            $validator = Validator::make($request->all(),[ 
                'user_id' => 'required|', 
                ]);
            if ($validator->fails()){ 
                return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);            
            }
            $user_id=$request->input("user_id");
            $vehicles=Vehicles::where('is_active',true)->where('added_by',$user_id)->get();
            if(!empty($vehicles)){
                $response['status'] = 200;
                $response['data'] = $vehicles;
                $response['message'] = "Success";
                return response()->json($response);
            }
            else{
                $response['status'] = 204;
                $response['message'] = "Success";
                return response()->json($response);

            } 
        }catch (\Exception $e) {
            $response['status'] = 500;
            $response['message'] = $e->getMessage();
            return response()->json($response);
        }
    }


    public function getVehicle(Request $request){
        try {
            $validator = Validator::make($request->all(),[ 
                'id' => 'required|', 
                ]);
            if ($validator->fails()){ 
                return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);            
            }
            $vehicle_id=$request->input("id");
            $vehicle = Vehicles::findOrFail($vehicle_id);
            if(!empty($vehicle)){
                $response['status'] = 200;
                $response['data'] = $vehicle;
                $response['message'] = "Success";
                return response()->json($response);
            }
            else{
                $response['status'] = 204;
                $response['data'] = [];
                $response['message'] = "Success";
                return response()->json($response);
            }
        } catch (\Exception $e){
            $response['status'] = 500;
            $response['message'] = $e->getMessage();
            return response()->json($response);
        }
    }
    public function addVehicle(Request $request){
        try {
            $validator = Validator::make($request->all(),[ 
                'user_id' => 'required|',
                'vehical_make' => 'required|', 
                'vehical_model' => 'required|',
                'vehical_number' => 'required|unique:'.with(new Vehicles)->getTable().',vehical_number',
                ]);
            if ($validator->fails()){ 
                return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);            
            }
            $input = array_map('trim', $request->all());
            $input['added_by']=$request->user_id;
            $input['is_active']=true;
            $vehicle = Vehicles::create($input);
            if($vehicle){
                $response['status'] = 200;
                $response['data'] = $vehicle;
                $response['message'] = "Vehicle added successfully";
                return response()->json($response);
            }
            else{
                $response['status'] = 500;
                $response['message'] = "Some thing wrong please try again later.";
                return response()->json($response);
            }
        } catch (\Exception $e) {
            $response['status'] = 500;
            $response['message'] = $e->getMessage();
            return response()->json($response);
        }
    }
    public function updateVehicle(Request $request){
        try {
            $validator = Validator::make($request->all(),[ 
                'id'=>'required',
                'vehical_make' => 'required', 
                'vehical_model' => 'required',
                'vehical_number' => 'required',
                ]);
            if ($validator->fails()){ 
                return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);            
            }
            
            $vehicle_id=$request->input("id");
            $vehicle = Vehicles::findOrFail($vehicle_id);
            $data = $request->all();
            $vehicle->update($data);
            if($vehicle){
                $response['status'] = 200;
                $response['data'] = $vehicle;
                $response['message'] = "Vehicle updated successfully";
                return response()->json($response);
            }
            else{
                $response['status'] = 500;
                $response['message'] = "Some thing wrong please try again later.";
                return response()->json($response);

            }
        }catch (\Exception $e) {
            $response['status'] = 500;
            $response['message'] = $e->getMessage();
            return response()->json($response);
        }
    }

}
