<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Auth;
use File;
use App\Complaint;

class ComplaintsController extends Controller
{
   public function getAllComplaints(Request $request){
    try{
        $complaints=Complaint::where('is_active',true)->get();
        if(!empty($complaints)){
            $response['status'] = 200;
            $response['data'] = $complaints;
            $response['message'] = "Success";
            return response()->json($response);
        }
        else{
            $response['status'] = 204;
            $response['data'] = [];
            $response['message'] = "Success";
            return response()->json($response);
    
        }
    }catch (\Exception $e){
        $response['status'] = 500;
        $response['message'] = $e->getMessage();
        return response()->json($response);
    }
   }
   public function addComplaint(Request $request){
    try{
        $validator = Validator::make($request->all(),[ 
            'user_id' => 'required',
            'location' => 'required', 
            'reason' => 'required',
            'comments' => 'required',
            'evidence_image' => 'nullable|mimes:jpg,bmp,png,jpeg',
            'evidence_video' => 'nullable|mimetypes:video/mp4,video/avi,video/mpeg,video/quicktime| max:20000',
            ]);
        if($validator->fails()){ 
            return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);            
        }
        $data = $request->all();
        // $user = Complaint::where('user_id',$request->user_id)->first();
        $evidence_image = $request->file('evidence_image');
        if($evidence_image){
            $path = public_path(config('constants.USERS_UPLOADS_PATH'));
            $evidence_imageName = time().'.'.$evidence_image->getClientOriginalExtension();
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            $uploadResponse = $evidence_image->move($path, $evidence_imageName);
            if(isset($user->evidence_image) && $user->evidence_image!='' && file_exists($path.$user->evidence_image)){
                File::delete($path.$user->evidence_image);
            }
            $data['evidence_image'] = $evidence_imageName;
        }
        $evidence_video = $request->file('evidence_video');
        if($evidence_video){
            $path = public_path(config('constants.USERS_UPLOADS_PATH'));
            $evidence_videoName = time().'.'.$evidence_video->getClientOriginalExtension();
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            $uploadResponse = $evidence_video->move($path, $evidence_videoName);
            if(isset($user->evidence_video) && $user->evidence_video!='' && file_exists($path.$user->evidence_video)){
                File::delete($path.$user->evidence_video);
            }
            $data['evidence_video'] = $evidence_videoName;
        }
        $data['is_active']=true;
        $data['status']=false;
        $complaint = Complaint::create($data);
        if($complaint){
            $response['status'] = 200;
            $response['data'] = $complaint;
            $response['message'] = "Complaint added successfully";
            return response()->json($response);
        }
        else{
            $response['status'] = 500;
            $response['message'] = "Some thing wrong please try again later.";
            return response()->json($response);
        } 
    }catch (\Exception $e){
        $response['status'] = 500;
        $response['message'] = $e->getMessage();
        return response()->json($response);
    }
   }
   public function getComplaint(Request $request){
    try{
        $validator = Validator::make($request->all(),[ 
            'id' => 'required', 
            ]);
        if ($validator->fails()){ 
            return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);            
        }
        $id=$request->input("id");
        $complaint=Complaint::where('is_active',true)->where('id',$id)->first();
        if(!empty($complaint)){
            $response['status'] = 200;
            $response['data'] = $complaint;
            $response['message'] = "Success";
            return response()->json($response);
        }
        else{
            $response['status'] = 204;
            $response['data'] = [];
            $response['message'] = "Success";
            return response()->json($response);
        }
    }catch (\Exception $e) {
        $response['status'] = 500;
        $response['message'] = $e->getMessage();
        return response()->json($response);
    }
   }
   public function getUserComplaints(Request $request){
    try{
        $validator = Validator::make($request->all(),[ 
            'user_id' => 'required', 
            ]);
        if ($validator->fails()){ 
            return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);            
        }
        $user_id=$request->input("user_id");
        $complaints=Complaint::where('is_active',true)->where('user_id',$user_id)->get();
        if(!empty($complaints)){
            $response['status'] = 200;
            $response['data'] = $complaints;
            $response['message'] = "Success";
            return response()->json($response);
        }
        else{
            $response['status'] = 204;
            $response['data'] = [];
            $response['message'] = "Success";
            return response()->json($response);
        }
    }catch (\Exception $e) {
        $response['status'] = 500;
        $response['message'] = $e->getMessage();
        return response()->json($response);
    }
   }
}
