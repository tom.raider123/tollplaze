<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Toll;
use Validator;
use Auth;
use File;

class TollsController extends Controller
{
    public function getNearbuyTolls(Request $request){
        try {
            $validator = Validator::make($request->all(),[ 
                'lat' => 'required', 
                'long' => 'required',
                ]);
            if ($validator->fails()){ 
                return response()->json(['status'=>400,'message'=>$validator->errors()->first()]);            
            }
            $tolls=Toll::where('toll_lat','!=',null)->where('toll_long','!=',null)->take(5)->get();
            if(!empty($tolls)){
                $response['status'] = 200;
                $response['data'] = $tolls;
                $response['message'] = "Success";
                return response()->json($response);
            }
            else{
                $response['status'] = 204;
                $response['message'] = "Success";
                return response()->json($response);

            } 
        }catch (\Exception $e) {
            $response['status'] = 500;
            $response['message'] = $e->getMessage();
            return response()->json($response);
        }


    }
}
