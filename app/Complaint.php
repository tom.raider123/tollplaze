<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $table = 'complaints';
    protected $fillable = [
        'user_id','location','reason','comments','evidence_image','evidence_voice','evidence_video','is_active','status'
    ];


    public function getuser(){
        return $this->belongsTo('App\User', 'user_id', 'id'); 
    }
}
