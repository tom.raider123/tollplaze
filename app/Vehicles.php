<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicles extends Model
{
    protected $fillable = [
        'vehical_make','vehical_model','vehical_number','vehical_image','is_active','added_by',
    ];

    public function getuser(){
        return $this->belongsTo('App\User', 'added_by', 'id'); 
    }
}
